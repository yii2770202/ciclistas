<?php

use yii\helpers\Html; // Agrega esta línea para incluir la clase Html
?>

<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-12">
                <div class="card alturaminima d-flex justify-content-center flex-wrap flex-row gap-3">
                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['ciclista/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['ciclista/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <p class="d-flex justify-content-center ">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 6</h3>
                        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 7</h3>
                        <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 8</h3>
                        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary botones']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 10</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                        <p class="d-flex justify-content-center gap-2">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                    <div class="card-body tarjeta border">
                        <h3 class="text-center">Consulta 11</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                        <p class="d-flex justify-content-center gap-2">
                            <?= Html::a('Active Record', ['site/consultala'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('DAO', ['site/consultala'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>