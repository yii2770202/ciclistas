<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $resultados yii\data\ActiveDataProvider */
/* @var $campos array */
/* @var $titulo string */
/* @var $enunciado string */
/* @var $sql string */

$this->title = $titulo;
$this->params['breadcrumbs'][] = ['label' => 'Ciclistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="resultado-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::encode($enunciado) ?></p>
    <p><?= Html::encode("Consulta SQL: $sql") ?></p>

    <?= GridView::widget([
        'dataProvider' => $resultados,
        'columns' => [
            'nombre',
            'edad',
        ],
    ]); ?>

</div>
